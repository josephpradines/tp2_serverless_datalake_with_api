# TODO : Create the lambda role with aws_iam_role

resource "aws_iam_role" "lambda_1_role_1" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# TODO : Create lambda function with aws_lambda_function

resource "aws_lambda_function" "lambda_func_1" {
  function_name = var.data_processing_lambda_lambda_name
  role          = aws_iam_role.lambda_1_role_1.arn
  filename      = "empty_lambda_code.zip"
  handler       = "lambda_main_app.lambda_handler"
  runtime       = "python3.7"
  depends_on    = [aws_iam_role_policy_attachment.lambda_logs]

}
# TODO : Create a aws_iam_policy for the logging, this resource policy will be attached to the lambda role

resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

# TODO : Attach the logging policy to the lambda role with aws_iam_role_policy_attachment

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.lambda_1_role_1.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

# TODO : Attach the AmazonS3FullAccess policy to the lambda role with aws_iam_role_policy_attachment

resource "aws_iam_role_policy_attachment" "s3_full" {
  role       = aws_iam_role.lambda_1_role_1.name
  policy_arn = var.s3_full_access_policy_arn
}
# TODO : Allow the lambda to be triggered by a s3 event with aws_lambda_permission

resource "aws_lambda_permission" "allow_s3_event" {
  statement_id  = "AllowExecutionFromS3Upload"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_func_1.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.s3Bucket_1.arn
}

