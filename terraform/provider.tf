# PROVIDER

provider "aws" {
  profile = "bertrand"
  region  = var.region
  version = "~> 3.9"
}

terraform {
  required_version = "= 0.13.5"
}
