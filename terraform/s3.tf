# TODO : Create a s3 bucket with aws_s3_bucket

#Note S3 Bucket Name !! ----------------
resource "aws_s3_bucket" "s3Bucket_1" {
  bucket        = var.s3_user_bucket_name2
  force_destroy = true
}

resource "aws_iam_role_policy_attachment" "AmazonS3FullAccess" {
  role       = aws_iam_role.lambda_1_role_1.name
  policy_arn = var.s3_full_access_policy_arn
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

resource "aws_s3_bucket_object" "Object_joboffers_raw" {
  bucket = aws_s3_bucket.s3Bucket_1.id
  key    = "job_offers/raw/"
  source = "/dev/null"
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3Bucket_1.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda_func_1.arn
    events = [
    "s3:ObjectCreated:*"]
    filter_prefix = "job_offers/raw/"
    filter_suffix = ".csv"
  }
  depends_on = [aws_lambda_permission.allow_s3_event]
}
