3.6.1 ______

![cloud_1](/uploads/0b95e8625ffedcb2a5ef286b79efad49/cloud_1.png)

![cloud_2](/uploads/8ce69c1c079e328403a85631b4c12e4a/cloud_2.png)

3.6.2 ______

At first, couldn't select Workgroup esme, had to specify the output out to :
` s3://<BUCKET_NAME>/athena_results`
The terraform athena template doesn't specify that a trailing slash is necessary, maybe causing this issue

![Screenshot_2020-11-07_Athena](/uploads/7ee756c613c61d97525b6321dfcca207/Screenshot_2020-11-07_Athena.png)

![Screenshot_2020-11-07_Athena_1_](/uploads/4326715c2281c534c1cfd4faa8270730/Screenshot_2020-11-07_Athena_1_.png)

![Screenshot_2020-11-08_Athena](/uploads/963018c16b0f5fdf7d9aaa5baaea6036/Screenshot_2020-11-08_Athena.png)

3.8 ________

![final_result](/uploads/b57fef4b89511fe8f6b25f736d12a414/final_result.PNG)